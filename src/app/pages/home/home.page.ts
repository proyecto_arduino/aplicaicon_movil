import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, PopoverController, AlertController, ModalController, ToastController } from '@ionic/angular';
import { ComunicationService } from 'src/app/services/comunication.service';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit{

  public humedad:number = 0;
  public humedadmenor:number = 100;
  public humedadmayor:number = 0;
  public fechahoramenor:any;
  public fechahoramayor:any;
  public fechahoactual:any;
  public canvas:boolean=true;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private comunicationService: ComunicationService
  ) {

  }
  
  ngOnInit(){
    this.comunicationService.getData().subscribe((data:any) => {
      let now = moment();
      let sacaT = now.format().split('T');
      this.fechahoactual = sacaT[0] + '  ' +  sacaT[1].split('-')[0];
      this.humedad = Number(data);
      if(this.humedad > this.humedadmayor){
        this.humedadmayor = this.humedad;
        this.fechahoramayor = this.fechahoactual;
      }
      if(this.humedadmenor > this.humedad){
        this.humedadmenor = this.humedad;
        this.fechahoramenor = this.fechahoactual;
      }
      
    })
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  ionViewWillLeave(){
 
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Change Location',
      message: 'Type your Address.',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Change',
          handler: async (data) => {
            console.log('Change clicked', data);
            const toast = await this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  // async searchFilter () {
  //   const modal = await this.modalCtrl.create({
  //     component: SearchFilterPage
  //   });
  //   return await modal.present();
  // }

  // async presentImage(image: any) {
  //   const modal = await this.modalCtrl.create({
  //     component: ImagePage,
  //     componentProps: { value: image }
  //   });
  //   return await modal.present();
  // }

  // async notifications(ev: any) {
  //   const popover = await this.popoverCtrl.create({
  //     component: NotificationsComponent,
  //     event: ev,
  //     animated: true,
  //     showBackdrop: true
  //   });
  //   return await popover.present();
  // }

}