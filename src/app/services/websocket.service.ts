import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  public status:boolean=false;

  constructor(
    private socket: Socket
  ) {
    this.conexion();
  }

  conexion(){

    this.socket.on('connect', () => {
      this.status = true;
    })

    this.socket.on('disconnect', () => {
      this.status = false;
    })
  }

  listen( evento:string){
    return this.socket.fromEvent(evento)
  }
}
