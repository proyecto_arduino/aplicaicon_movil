import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';

@Injectable({
  providedIn: 'root'
})
export class ComunicationService {

  constructor(
    public socket: WebsocketService) { }

  getData(){
    return this.socket.listen('mensaje-nuevo');
  }
}
